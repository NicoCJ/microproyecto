using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Dolor : MonoBehaviour
{

    // Variable para almacenar la posici�n de destino
    public Transform destino;

    // M�todo que se llama cuando otro collider entra en contacto con este collider
    private void OnTriggerEnter2D(Collider2D other)
    {
        // Comprobamos si el collider que entr� en contacto es el jugador
        if (other.CompareTag("Player"))
        {
            // Movemos al jugador a la posici�n de destino
            other.transform.position = destino.position;
        }
    }
}
