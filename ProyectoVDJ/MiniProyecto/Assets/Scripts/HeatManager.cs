using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HeatManager : MonoBehaviour
{

    private void Update()
    {
        AllHeatsCollected();
    }

    public void AllHeatsCollected()
    {
        if(transform.childCount == 0)
        {
            Debug.Log("Haz recolectado todas las frutas, felicidades");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
        }
    }

}
