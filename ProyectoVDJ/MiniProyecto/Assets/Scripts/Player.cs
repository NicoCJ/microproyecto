using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.WSA;

public class Player : MonoBehaviour
{
    //        NORMAL JUMP
    public float runSpeed = 2;
    public float jumpSpeed = 3;

    //        REFERENCES
    Rigidbody2D rb2D;

    //VARIABLES FOR BETTER JUMP 
    public bool betterJump = false;
    public float falMultiplier = 0.2f;
    public float lowJumpMultiplier = 2f;

    // ASIGMENT IN INSPECTOR
    public SpriteRenderer spriteRenderer;
    public Animator animator;

    

    void Start()
    {

        UnityEngine.Cursor.visible = false;
        UnityEngine.Cursor.lockState = CursorLockMode.Locked;

        rb2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
     
        //                      MOVEMENT 
        if (Input.GetKey("d") || Input.GetKey("right"))
        {
            rb2D.velocity = new Vector3(runSpeed, rb2D.velocity.y);
            spriteRenderer.flipX = false;
            animator.SetBool("Run",true);
        }
        else if (Input.GetKey("a") || Input.GetKey("left"))
        {
            rb2D.velocity = new Vector3(-runSpeed, rb2D.velocity.y);
            spriteRenderer.flipX = true;
            animator.SetBool("Run", true);
        }
        else 
        {
            rb2D.velocity = new Vector3(0, rb2D.velocity.y);
            animator.SetBool("Run", false);
        }

        //                      SALTO
        if (Input.GetKey("space") && CheckGround.isGrounded)
        {
            rb2D.velocity = new Vector2(rb2D.velocity.x, jumpSpeed);
        }
        if(CheckGround.isGrounded == false)
        {
            animator.SetBool("Jump", true);
            animator.SetBool("Run", false);
        }
        if (CheckGround.isGrounded == true)
        {
            animator.SetBool("Jump", false);
        }

        if (betterJump)
        {
            if (rb2D.velocity.y < 0)
            {
                rb2D.velocity += Vector2.up * Physics2D.gravity.y * (falMultiplier) * Time.deltaTime;
            }
            if (rb2D.velocity.y > 0 && !Input.GetKey("space"))
            {
                rb2D.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier) * Time.deltaTime;
            }
        }

        }

    }
  


